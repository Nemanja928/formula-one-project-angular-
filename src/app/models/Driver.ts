export class Driver {
    position: number;
    givenName: string;
    familyName: string;
    points: number;
}