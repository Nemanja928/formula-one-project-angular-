import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { DriverListComponent } from './components/driver-list/driver-list.component';
import { TeamListComponent } from './components/team-list/team-list.component';
import { RaceListComponent } from './components/race-list/race-list.component';
import { DriverListDetailComponent } from './components/driver-list-detail/driver-list-detail.component';
import { TeamDetailComponent } from './components/team-detail/team-detail.component';
import { RaceDetailComponent } from './components/race-detail/race-detail.component';
import { DriverChartsComponent } from './components/driver-charts/driver-charts.component';
import { TeamChartsComponent } from './components/team-charts/team-charts.component';
import { LoadingSpinnerComponent } from './components/ui/loading-spinner/loading-spinner.component';
import { DataService } from './services/data.service';
const appRoutes = [
{path: '', component: DriverListComponent},
{path: 'driver/:driverId', component: DriverListDetailComponent},
{path: 'team', component: TeamListComponent},
{path: 'team/:constructorId', component: TeamDetailComponent},
{path: 'race', component: RaceListComponent},
{path: 'race/:round', component: RaceDetailComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    DriverListComponent,
    TeamListComponent,
    RaceListComponent,
    DriverListDetailComponent,
    TeamDetailComponent,
    RaceDetailComponent,
    DriverChartsComponent,
    TeamChartsComponent,
    LoadingSpinnerComponent
  
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
