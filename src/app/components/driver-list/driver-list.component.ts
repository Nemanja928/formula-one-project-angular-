import { Component, OnInit } from '@angular/core';
import { Driver } from '../../models/Driver';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['./driver-list.component.css']
})
export class DriverListComponent implements OnInit {
  showSpinner: boolean = true;
  drivers: Driver[];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getDrivers().subscribe(drivers => {
      this.showSpinner = false;
      this.drivers = drivers.MRData.StandingsTable.StandingsLists[0].DriverStandings;
      
    })


  }

}
