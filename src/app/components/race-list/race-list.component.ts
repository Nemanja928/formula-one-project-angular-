import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Race } from 'src/app/models/Race';

@Component({
  selector: 'app-race-list',
  templateUrl: './race-list.component.html',
  styleUrls: ['./race-list.component.css']
})
export class RaceListComponent implements OnInit {
  showSpinner: boolean = true;
  races: Race[];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getRace().subscribe(races => {
      this.showSpinner = false;
      this.races = races.MRData.RaceTable.Races;
      //console.log(this.races);
    })
  }

}
