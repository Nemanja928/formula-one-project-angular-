import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DataService } from 'src/app/services/data.service';
import { DriverDetails } from 'src/app/models/DriverDetails';

@Component({
  selector: 'app-driver-list-detail',
  templateUrl: './driver-list-detail.component.html',
  styleUrls: ['./driver-list-detail.component.css']
})
export class DriverListDetailComponent implements OnInit {
  showSpinner: boolean = true;
  drivers: DriverDetails[];
  constructor(private dataService: DataService, private route: ActivatedRoute) { }
 
 

  ngOnInit() {
    
    this.dataService.getDriverDetails(this.route.snapshot.params['driverId']).subscribe(drivers => {
      this.showSpinner = false;
      this.drivers = drivers.MRData.RaceTable.Races;
     // console.log(this.drivers);
    })
  }

}