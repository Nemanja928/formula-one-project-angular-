import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/models/Team';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
  showSpinner: boolean = true;
teams: Team[];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getTeams().subscribe(teams => {
      this.showSpinner = false;
     this.teams = teams.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
      console.log(teams);
    })
  }

}
