import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-team-charts',
  templateUrl: './team-charts.component.html',
  styleUrls: ['./team-charts.component.css']
})
export class TeamChartsComponent implements OnInit {
  chart = [];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getTeams().subscribe(teams => {
      
      let teamName = teams.MRData.StandingsTable['StandingsLists'][0]['ConstructorStandings'].map(teams => teams.Constructor.name);
      let points = teams.MRData.StandingsTable['StandingsLists'][0]['ConstructorStandings'].map(teams => teams.points);
      //  console.log(teamName);
      //  console.log(points);
      this.chart = new Chart('canvas', {
       type: 'bar',
       data: {
         labels: teamName,
         datasets: [
           { 
             data: points,
             backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9", '#87e1fb', '#ba8d81', '#ba70d4', 
             '#f477c6', '#2dc3a4', '#062cdb', '#dccc31', '#415675', '#fa9b02', '#d82056', '#df1072', '#d9d4aa', '#004d79'],
            borderColor: 'red',
            borderWidth: 3,
             fill: false
           },
           
         ]
       },
       options: {
         title: {
           display: true,
           text: 'Team points - Season 2013'
         },
         legend: {
           display: false
         },
         scales: {
           xAxes: [{
             display: true
           }],
           yAxes: [{
             display: true
           }],
         }
       }
     });
     })
     
     
   }
 
 }