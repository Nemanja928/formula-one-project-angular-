import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-driver-charts',
  templateUrl: './driver-charts.component.html',
  styleUrls: ['./driver-charts.component.css']
})
export class DriverChartsComponent implements OnInit {
  chart = [];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getDrivers().subscribe(drivers => {
      
     let driverName = drivers.MRData.StandingsTable['StandingsLists'][0]['DriverStandings'].map(drivers => drivers.Driver.familyName);
     let points = drivers.MRData.StandingsTable['StandingsLists'][0]['DriverStandings'].map(drivers => drivers.points);
    //  console.log(driverName);
    //  console.log(points);
     this.chart = new Chart('canvas', {
      type: 'bar',
      data: {
        labels: driverName,
        datasets: [
          { 
            data: points,
            backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9", '#87e1fb', '#ba8d81', '#ba70d4', 
            '#f477c6', '#2dc3a4', '#062cdb', '#dccc31', '#415675', '#fa9b02', '#d82056', '#df1072', '#d9d4aa', '#004d79'],
           borderColor: 'red',
           borderWidth: 2,
            fill: false
          },
          
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Driver points - Season 2013'
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
    })
    
    
  }

}