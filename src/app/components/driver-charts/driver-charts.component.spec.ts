import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverChartsComponent } from './driver-charts.component';

describe('DriverChartsComponent', () => {
  let component: DriverChartsComponent;
  let fixture: ComponentFixture<DriverChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
