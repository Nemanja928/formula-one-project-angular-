import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { RaceDetails } from 'src/app/models/race-details';

@Component({
  selector: 'app-race-detail',
  templateUrl: './race-detail.component.html',
  styleUrls: ['./race-detail.component.css']
})
export class RaceDetailComponent implements OnInit {
  showSpinner: boolean = true;
races: RaceDetails[];
racesQ: RaceDetails[];
  constructor(private dataService: DataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.dataService.getRaceDetailsQ(this.route.snapshot.params['round']).subscribe(races => {
      this.showSpinner = false;
      this.racesQ = races.MRData.RaceTable.Races;
      //console.log(this.racesQ);
    })
    this.dataService.getRaceDetailsR(this.route.snapshot.params['round']).subscribe(races => {
      this.showSpinner = false;
      this.races = races.MRData.RaceTable.Races;
     // console.log(this.races);
    })

  }

}
