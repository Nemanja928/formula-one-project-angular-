import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { TeamDetail } from 'src/app/models/team-detail';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {
  showSpinner: boolean = true;
  teams: TeamDetail[];
  constructor(private dataService: DataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.dataService.getTeamDetail(this.route.snapshot.params['constructorId']).subscribe(teams => {
      this.showSpinner = false;
      this.teams = teams.MRData.RaceTable.Races;
      console.log(this.teams);
    })
  }

}
