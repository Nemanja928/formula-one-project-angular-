import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

import { Driver } from '../models/Driver';
import { Team } from '../models/Team';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  private rootUrl = 'https://ergast.com/api/f1/2013/';

  getDrivers():Observable<any> {
    return this.http.get<any>(this.rootUrl + 'driverStandings.json')
    .pipe(map(result => result));
  }
  getDriverDetails(id: any):Observable<any> {
    
    return this.http.get<any>('http://ergast.com/api/f1/2013/' + id + '/results.json');
  }
  getTeams():Observable<any> {
    return this.http.get<any>(this.rootUrl + 'constructorStandings.json');
  }
  getTeamDetail(id: any):Observable<any> {
    return this.http.get<any>(this.rootUrl + 'constructors/' + id + '/results.json');
  }
  
  getRace():Observable<any> {
    return this.http.get<any>(this.rootUrl + 'results/1.json');
  }
  getRaceDetailsQ(id: any):Observable<any> {
    return this.http.get<any>(this.rootUrl + id + '/qualifying.json');
  }
  getRaceDetailsR(id: any):Observable<any> {
    return this.http.get<any>(this.rootUrl + id + '/results.json');
  }
}
